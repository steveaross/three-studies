const path = require('path');

module.exports = {
    entry: {
        main: './src/index.js',
        time: './src/time.js',
        move: './src/move.js',
        copresence: './src/copresence.js'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js',
    },
    devServer: {
        static: {
            directory: path.resolve(__dirname, 'dist'),
            publicPath: '/'
        }
    },
    mode: 'development'
};