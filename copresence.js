import * as THREE from 'three';

import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader.js';

import { v4 as uuidv4 } from 'uuid';
import { connect, credsAuthenticator, JSONCodec } from "nats.ws";

import { initializeApp } from 'firebase/app';
import { getAnalytics } from "firebase/analytics";
const firebaseConfig = {
    apiKey: "AIzaSyByy7tBWW9NzNKudMTWbtVAY2U4ieGE8ho",
    authDomain: "sunny-catalyst-329003.firebaseapp.com",
    projectId: "sunny-catalyst-329003",
    storageBucket: "sunny-catalyst-329003.appspot.com",
    messagingSenderId: "300665995890",
    appId: "1:300665995890:web:5991250fadd9130ca0f8eb",
    measurementId: "G-17BQ238EZW"
};

// Get a list of cities from your database
async function getCities(db) {
    const citiesCol = collection(db, 'cities');
    const citySnapshot = await getDocs(citiesCol);
    const cityList = citySnapshot.docs.map(doc => doc.data());
    return cityList;
}

function getMilliseconds() {
    return (new Date()).getTime() / 1000
}

let camera, scene, renderer, selfmesh, goal, keys, follow;

let temp = new THREE.Vector3;
let dir = new THREE.Vector3;
let a = new THREE.Vector3;
let b = new THREE.Vector3;
let clock = new THREE.Clock(true);
let coronaSafetyDistance = 0.3;
let velocity = 0.0;
let speed = 0.0;
let controls;
let plane;
let arrowobj;
let heartobj;

const texloader = new THREE.TextureLoader();
const objloader = new OBJLoader();

let userid
let userstate
let allusers

let nc;
const jc = JSONCodec();

(async () => {
    await startcomms();
})();

function init() {
    renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.shadowMap.enabled = true
    renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(renderer.domElement);


    scene = new THREE.Scene();

    let ambient = new THREE.AmbientLight(0xffffff); // soft white light
    ambient.intensity = 0.6
    scene.add(ambient);

    camera = new THREE.PerspectiveCamera(70, window.innerWidth / window.innerHeight, 0.01, 10);
    var geometry = new THREE.BoxBufferGeometry(0.1, 0.1, 0.1);
    var material = new THREE.MeshStandardMaterial({ color: userstate.c });


    const lamp = new THREE.PointLight(new THREE.Color(0xffffff))
    lamp.intensity = 0.2
    lamp.position.set(0, 2, 0)
    scene.add(lamp)

    const sun = new THREE.DirectionalLight(new THREE.Color(0xffffff))
    sun.intensity = 0.2
    scene.add(sun)

    sun.castShadow = true
    sun.shadow.radius = 1
    sun.shadow.mapSize.width = 2048;
    sun.shadow.mapSize.height = 2048;

    const d = 3

    sun.shadow.camera.left = - d;
    sun.shadow.camera.right = d;
    sun.shadow.camera.top = d;
    sun.shadow.camera.bottom = - d;
    sun.shadow.zoom = 0.1

    sun.shadow.camera.far = 3500;
    sun.shadow.bias = 0;


    let geo = new THREE.PlaneGeometry(30, 30)
    let tex = texloader.load('/grass-3.jpg')
    tex.wrapS = THREE.RepeatWrapping;
    tex.wrapT = THREE.RepeatWrapping;
    tex.repeat.set(10, 10)
    let mat = new THREE.MeshStandardMaterial({ map: tex })
    plane = new THREE.Mesh(geo, mat)
    plane.receiveShadow = true
    plane.position.set(0, -0.15, 0)
    plane.rotation.set(Math.PI / -2, 0, 0)
    scene.add(plane)

    scene.background = new THREE.Color(0xeaf3fa)
    scene.fog = new THREE.Fog(scene.background, 1, 10);

    controls = new OrbitControls(camera, renderer.domElement);
    controls.enablePan = false;
    controls.enableDamping = true;

    keys = {
        a: false,
        s: false,
        d: false,
        w: false
    };

    document.body.addEventListener('touchstart', function (e) {
        placeReaction()
    });

    document.body.addEventListener('keydown', function (e) {

        const key = e.code.replace('Key', '').toLowerCase();
        if (keys[key] !== undefined)
            keys[key] = true;

        if (key == "space") {
            placeReaction()
        }
    });
    document.body.addEventListener('keyup', function (e) {

        const key = e.code.replace('Key', '').toLowerCase();
        if (keys[key] !== undefined)
            keys[key] = false;

    });


    objloader.load(
        'arrow.vox.obj',
        function (o) {
            arrowobj = o
            arrowobj.position.set(0, -0.15, 0)
            let s = 0.05
            arrowobj.scale.set(s, s, s)
            arrowobj.castShadow = true

            selfmesh = arrowobj.clone()
            recolorObj(selfmesh, userstate.c)
            selfmesh.position.set(userstate.p.x, -0.15, userstate.p.y)
            selfmesh.rotateY(userstate.rot)

            camera.lookAt(selfmesh.position)
            camera.position.sub(selfmesh.position).setLength(0.5).add(selfmesh.position);

            scene.add(selfmesh);
        },
        function (xhr) {
            console.log((xhr.loaded / xhr.total * 100) + '% loaded');
        },
        function (error) {
            console.log(error)
        }
    );

    objloader.load(
        'heart.vox.obj',
        function (o) {
            heartobj = o
            heartobj.castShadow = true
            heartobj.position.set(0, -0.15, 0)
            let s = 0.1
            heartobj.scale.set(s, s, s)
        },
        function (xhr) {
            console.log((xhr.loaded / xhr.total * 100) + '% loaded');
        },
        function (error) {
            console.log(error)
        }
    )

}

let reactions = []

function placeReaction(id) {
    let color
    let rot
    let pos
    if (!id) {
        color = userstate.c
        pos = selfmesh.position
        rot = selfmesh.rotation.y
    } else {
        if (!allusers[id] || !otherboxes[id]) {
            return
        }
        color = allusers[id].c
        pos = otherboxes[id].position
        rot = otherboxes[id].rotation.y
    }
    let h = heartobj.clone()
    recolorObj(h, color)
    h.position.set(pos.x, pos.y + 0.1, pos.z)
    h.rotation.y = rot

    scene.add(h)

    reactions.push({
        age: 0,
        obj: h
    })

    if (!id && nc) {
        let msg = {
            u: userid,
            a: 'heart'
        }
        nc.publish("things.reactions", jc.encode(msg))
    }
}

let otherboxes = {}

function recolorObj(o, color) {
    o.traverse(function (child) {
        if (child.isMesh) {
            child.material = new THREE.MeshStandardMaterial({ color: color });
            child.castShadow = true
        }
    });
}

function renderOtherBoxes(scene) {
    if (!arrowobj) {
        return
    }
    for (const id in broadcastdata) {
        if (id == userid) {
            continue
        }
        let u = broadcastdata[id]
        if (!u || !u.p) {
            continue
        }
        let b
        let color = allusers[id].c
        if (!otherboxes[id]) {
            console.log(`ADD ${id}`)
            b = arrowobj.clone()
            recolorObj(b, color)
            b.position.set(0, Math.random() * 0.001 - 0.15, 0)
            scene.add(b)
            placeReaction(id)
            otherboxes[id] = b
        } else {
            b = otherboxes[id]
        }
        b.position.x = u.p.x
        b.position.z = u.p.y
        b.rotation.y = u.rot
        const sleepheight = -0.27
        if (u.update < (getMilliseconds() - 60)) {
            recolorObj(b, (new THREE.Color(color)).lerp(new THREE.Color(0x999999), 0.9))
            b.position.y = sleepheight
        } else if (b.position.y == sleepheight) {
            recolorObj(b, new THREE.Color(color))
            b.position.y = Math.random() * 0.001 - 0.15
        }
    }
}

function extractRot(mesh) {
    return (new THREE.Euler().setFromQuaternion(mesh.quaternion, 'YXZ')).y
}

function updatePosition() {
    if (selfmesh.position.x != userstate.p.x || selfmesh.position.z != userstate.p.y || userstate.rot != extractRot(selfmesh)) {
        userstate.p.x = selfmesh.position.x
        userstate.p.y = selfmesh.position.z
        userstate.rot = extractRot(selfmesh)
        updateState()
    }
}

let broadcaststate = {}

function jsonEqual(a, b) {
    return JSON.stringify(a) === JSON.stringify(b);
}
let broadcastdata = {}

function broadcastPosition() {
    let newbroadcastdata = {
        u: userid,
        p: {
            x: selfmesh.position.x,
            y: selfmesh.position.z
        },
        rot: extractRot(selfmesh)
    }
    let comparedata = broadcastdata[userid] || {}
    delete comparedata.update
    if (nc && JSON.stringify(newbroadcastdata) != JSON.stringify(comparedata)) {
        nc.publish("things.moves", jc.encode(newbroadcastdata))
    }
}

let lastpositioncheck = 0
let debug = false
let lastbroadcast = 0

function animate() {
    requestAnimationFrame(animate);

    controls.update()

    if (selfmesh) {
        if (lastpositioncheck < Math.floor(clock.getElapsedTime())) {
            lastpositioncheck = Math.floor(clock.getElapsedTime())
            if (debug) {
                console.log("----")
                console.log(`self - ${selfmesh.position.x}, ${selfmesh.position.y}, ${selfmesh.position.z}, ${selfmesh.rotation.yd}`)
                for (const i in otherboxes) {
                    const b = otherboxes[i];
                    console.log(`${i} - ${b.position.x}, ${b.position.y}, ${b.position.z}`)
                }
            }
            updatePosition()
        }
        if (lastbroadcast < clock.getElapsedTime() - 0.01) {
            lastbroadcast = clock.getElapsedTime()
            broadcastPosition()
        }
        renderOtherBoxes(scene)

        speed = 0.0;
        let cameradistance = camera.position.distanceTo(selfmesh.position)
        let lastx = selfmesh.position.x
        let lastz = selfmesh.position.z

        if (keys.w)
            speed = 0.01;
        else if (keys.s)
            speed = -0.01;

        velocity += (speed - velocity) * .3;
        selfmesh.translateZ(velocity);

        if (keys.a) {
            selfmesh.rotateY(0.05);
        } else if (keys.d) {
            selfmesh.rotateY(-0.05);
        }

        controls.target = selfmesh.position;
        camera.lookAt(selfmesh.position);
        camera.position.x += (selfmesh.position.x - lastx)
        camera.position.z += (selfmesh.position.z - lastz)
        camera.position.sub(selfmesh.position).setLength(cameradistance).add(selfmesh.position);
        if (camera.position.y <= 0.15) {
            camera.position.y = 0.15
        }
    }

    for (let i = 0; i < reactions.length; i++) {
        const r = reactions[i];
        if (r.age == 100) {
            scene.remove(r.obj)
        }
        r.obj.position.y += 0.005
        r.age += 1
    }

    renderer.render(scene, camera);

}

function setTitle(text) {
    document.querySelector('#title').innerText = text
}

const ngs_creds = `
-----BEGIN NATS USER JWT-----
eyJ0eXAiOiJKV1QiLCJhbGciOiJlZDI1NTE5LW5rZXkifQ.eyJqdGkiOiJNNzZPTldQV0FVSlRLTVBDNlRCN1E0M1VFRkMzNDRNSEU3VVdTU1NIWldWVlMzU1I3VlVBIiwiaWF0IjoxNjM0NDIzMTk4LCJpc3MiOiJBQTZIRE4yUkJNTVc3SFVXWk1YSEFVWFRHS0lWTDJXV0lZRjdJUDVIS0VBSEs0M0NRRFJMSk9BRyIsIm5hbWUiOiJjbGllbnQiLCJzdWIiOiJVQVZFVk5KS0lORVlWNkNKTEEyVUZXVkdBNVVaTkZOM0VIUkxHUlVGTzQ0VDRQUUpUNDQzTUNZVyIsIm5hdHMiOnsicHViIjp7fSwic3ViIjp7fSwic3VicyI6LTEsImRhdGEiOi0xLCJwYXlsb2FkIjotMSwidHlwZSI6InVzZXIiLCJ2ZXJzaW9uIjoyfX0.uwKKrAF3qxn_6afx-ouLxECacxA2ttuIgJ9fhOGNYny9Axx77F_fQTX9ElcfTzdcaWqDeu6MffDmStEXQlP2BQ
------END NATS USER JWT------

************************* IMPORTANT *************************
NKEY Seed printed below can be used to sign and prove identity.
NKEYs are sensitive and should be treated as secrets.

-----BEGIN USER NKEY SEED-----
SUALTF25FOMJRRXL6FYVP7ME2L3GE7Z2FS4NMCZVL5BQU6MO743EPZU52A
------END USER NKEY SEED------

*************************************************************
`

import { getFirestore, collection, doc, onSnapshot, getDoc, getDocs, query, where, setDoc, updateDoc } from 'firebase/firestore';

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

let updatestateinprogress = false
async function updateState() {
    if (updatestateinprogress) {
        return
    }
    if (!db) {
        return
    }
    if (!userstate) {
        return
    }
    updatestateinprogress = true
    userstate.update = getMilliseconds()
    let stateupdate = {}
    stateupdate[`users.${userid}`] = userstate
    await updateDoc(doc(db, "copresence-study", "state"), stateupdate);
    //await sleep(2000); //optionally slow down update rate
    updatestateinprogress = false
}

let started = false
let db

async function startcomms() {
    userid = localStorage.getItem('userid')
    if (!userid) {
        userid = uuidv4()
        localStorage.setItem('userid', userid);
    }
    console.log(`userid ${userid}`)

    let res
    try {
        res = await fetch("https://meet.steve1.workers.dev/connect")
        if (res.status != 200) {
            throw new Error("not 200")
        }
    } catch (e) {
        setTitle("Cannot connect. You're all alone :(")
        return
    }
    setTitle("Connected.")

    const app = initializeApp(firebaseConfig);
    const analytics = getAnalytics(app);
    db = getFirestore(app)

    const stateref = doc(db, "copresence-study", "state")
    const d = await getDoc(stateref)

    if (!d.data()["users"][userid]) {
        userstate = {
            p: {
                x: Math.random() * 2,
                y: Math.random() * 2,
            },
            rot: 0,
            c: Math.floor(Math.random() * 16777215)
        }
        updateState()
    } else {
        userstate = d.data()["users"][userid]
        updateState()
    }
    broadcastdata = d.data()["users"]

    const unsub = onSnapshot(
        stateref,
        { includeMetadataChanges: true },
        (d) => {
            allusers = d.data()["users"]
            userstate = allusers[userid]
            if (!started) {
                started = true
                init();
                animate();
            }
            //console.log("got doc update")
        });

    try {
        console.log("starting a thing")
        nc = await connect({
            servers: 'connect.ngs.global',
            authenticator: credsAuthenticator(new TextEncoder().encode(ngs_creds))
        })
        console.log("did a thing")
        const sub = await nc.subscribe("things.moves");
        (async (sub) => {
            try {
                console.log(`listening for ${sub.getSubject()} requests...`);
                for await (const m of sub) {
                    let d = jc.decode(m.data)
                    d.update = getMilliseconds()
                    broadcastdata[d.u] = d
                }
                console.log(`subscription ${sub.getSubject()} drained.`);
            } catch (e) {
                setTitle("Visitor sync failed.")
                console.log(e)
            }
        })(sub);
        const sub2 = await nc.subscribe("things.reactions");
        (async (sub) => {
            try {
                console.log(`listening for ${sub.getSubject()} requests...`);
                placeReaction()
                for await (const m of sub) {
                    let d = jc.decode(m.data)
                    placeReaction(d.u)
                }
                console.log(`subscription ${sub.getSubject()} drained.`);
            } catch (e) {
                setTitle("Visitor sync failed.")
                console.log(e)
            }
        })(sub2);
        console.log("did more things")
    } catch (e) {
        setTitle("Visitor sync failed.")
        console.log(e)
        return
    }
    setTitle("In-sync.")
}